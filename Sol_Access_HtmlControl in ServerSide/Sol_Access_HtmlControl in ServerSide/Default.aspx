﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Access_HtmlControl_in_ServerSide.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="scriptManager" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">

            <ContentTemplate>

                <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" placeHolder="Name" style="background-color:yellow"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <span id="spanName" runat="server"></span>
                            
                            <asp:Button ID="btnSubmit" Text="Submit" runat="server" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
